based on:
https://www.baeldung.com/securing-a-restful-web-service-with-spring-security

```
#login as user
curl -i -X POST -d username=user -d password=user -c cookies.txt http://localhost:8080/login

#login as admin
curl -i -X POST -d username=admin -d password=admin -c cookies.txt http://localhost:8080/login

#call user endpoint
curl -i --header "Accept:application/json" -X GET -b cookies.txt http://localhost:8080/api/foos

#call admin endpoints
curl -i --header "Accept:application/json" -X GET -b cookies.txt http://localhost:8080/api/admin/hello
curl -i --header "Accept:application/json" -X GET -b cookies.txt http://localhost:8080/api/admin/user

#call secured admin endpoint
curl -i --header "Accept:application/json" -X GET -b cookies.txt http://localhost:8080/api/secured

# logout
curl -i --header "Accept:application/json" -X GET -b cookies.txt http://localhost:8080/logout

```
