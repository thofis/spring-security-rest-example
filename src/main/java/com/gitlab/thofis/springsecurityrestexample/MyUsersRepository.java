package com.gitlab.thofis.springsecurityrestexample;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MyUsersRepository extends JpaRepository<Users, Long> {
  Users findByUsername(String username);
}
