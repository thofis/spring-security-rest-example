package com.gitlab.thofis.springsecurityrestexample;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@SpringBootApplication
public class SpringSecurityRestExampleApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringSecurityRestExampleApplication.class, args);
  }

}

@Component
@RequiredArgsConstructor
class DBSetup implements ApplicationRunner {

  private final EntityManager entityManager;
  private final PasswordEncoder passwordEncoder;

  @Value("${initialuser.username}")
  private String username;

  @Value("${initialuser.username}")
  private String password;

  @Override
  @Transactional
  public void run(ApplicationArguments args) throws Exception {
    Query query = entityManager.createQuery("SELECT count(*) FROM Users");
    long count = (long) query.getSingleResult();
    if (count == 0) {
      Users user = Users.builder()
          .username(username)
          .password(passwordEncoder.encode(password))
          .enabled(true)
          .build();
      Authorities authority = Authorities.builder()
          .authority("ROLE_ADMIN")
          .user(user)
          .build();
      entityManager.persist(user);
      entityManager.persist(authority);
      user = Users.builder()
          .username("user")
          .password(passwordEncoder.encode("user"))
          .enabled(true).build();
      authority = Authorities.builder()
          .authority("ROLE_USER")
          .user(user).build();
      entityManager.persist(user);
      entityManager.persist(authority);



    }
  }
}
