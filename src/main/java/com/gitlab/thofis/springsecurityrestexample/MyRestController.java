package com.gitlab.thofis.springsecurityrestexample;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MyRestController {

  @GetMapping("/foos")
  public String hello() {
    return "Hello";
  }

  @GetMapping("/admin/hello")
  public String helloAdmin() {
    return "Hello Admin";
  }

  @GetMapping("/admin/user")
  public MyUserDetails user(@AuthenticationPrincipal MyUserDetails user) {
    return user;
  }

  @GetMapping("secured")
  @Secured("ROLE_ADMIN")
  public String sec() {
    return "secured";
  }

}

