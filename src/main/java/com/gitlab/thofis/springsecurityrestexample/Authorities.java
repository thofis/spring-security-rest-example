package com.gitlab.thofis.springsecurityrestexample;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "user")
public class Authorities implements GrantedAuthority {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String authority;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "user_id")
  private Users user;

}
